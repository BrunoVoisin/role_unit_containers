CI_REGISTRY ?= registry.gitlab.com
CI_PROJECT_PATH ?= role_unit/role_unit_containers
TARGETS = $(wildcard archlinux debian[0-9]* centos[0-9]* build)
ARCHLINUX_TARGETS = archlinux_cached
DEBIAN_TARGETS = $(patsubst %,%_cached,$(wildcard debian[0-9]*))
CENTOS_TARGETS = $(patsubst %,%_cached,$(wildcard centos[0-9]*))

containers: $(TARGETS)

$(TARGETS): login
	echo $@
	docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ -f $@/dockerfile $@
	test "${CI_COMMIT_REF_NAME}" == "master" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ || true

archlinux_cached: login
	echo $@
	docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ -f $@/dockerfile $@
	test "${CI_COMMIT_REF_NAME}" == "master" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ || true

$(DEBIAN_TARGETS): login
	echo $@
	docker build --build-arg DEBIAN_VERSION=$$(echo $@ | sed 's/_cached$$//') -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ -f debian_cached/dockerfile debian_cached
	test "${CI_COMMIT_REF_NAME}" == "master" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ || true

$(CENTOS_TARGETS): login
	echo $@
	docker build --build-arg CENTOS_VERSION=$$(echo $@ | sed 's/_cached$$//') -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ -f debian_cached/dockerfile centos_cached
	test "${CI_COMMIT_REF_NAME}" == "master" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ || true

login:
	test -n "${CI_BUILD_TOKEN}" && docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} || true

.PHONY: $(TARGETS)
